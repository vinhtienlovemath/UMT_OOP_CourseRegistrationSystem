#include "../include/Admin.h"

Admin::Admin() { }
Admin::Admin(std::string username, std::string password, std::string id): User(id, username, password) {}

std::vector<Student> Admin::GetMasterRegistry() {
    return master_registry_;
}

void Admin::SetUsername(std::vector<Student> master_registry) {
    this->master_registry_ = master_registry;
}

void Admin::CreateCourse() {
    std::string course_id;
    int course_section;
    std::cout << "Enter the course ID: ";
    std::cin >> course_id;
    std::cout << "Enter the course section number: ";
    std::cin >> course_section;

    for(int i = 0; i < course_list_.size(); ++i) {
        Course course_ith = course_list_[i];
        if(course_ith.GetCourseID() == course_id && course_ith.GetCourseSection() == course_section) {
            std::cout << "Oops! Duplicate with available course, let's enter another course!" << std::endl;
            course_ith.AdminPrint();
            CreateCourse();
        } 
        else {
            if(i == course_list_.size() - 1) {
                std::string course_name;
                int course_credits;
                std::string course_start_date;
                std::string course_end_date;
                int sessions_per_week;
                std::vector<Timeslot> timeslot;
                std::string course_location;
                std::string instructor_name;
                int max_students;
                int current_students = 0;
                std::vector<Student> student_list;

                std::cout << "Enter the course name: ";
                std::cin >> course_name;
                std::cout << "Enter the number of credits: ";
                std::cin >> course_credits;
                std::cout << "Enter the course start date: ";
                std::cin >> course_start_date;
                std::cout << "Enter the course end date: ";
                std::cin >> course_end_date;
                std::cout << "Enter the number of sessions per week: ";
                std::cin >> sessions_per_week;
                for(int i = 0; i < sessions_per_week; ++i) {
                    std::string course_dow;
                    std::string course_start_time;
                    std::string course_end_time;
                    std::cout << "Enter the day of week: ";
                    std::cin >> course_dow;
                    std::cout << "Enter the course start time: ";
                    std::cin >> course_start_time;
                    std::cout << "Enter the course end time: ";
                    std::cin >> course_end_time;
                    Timeslot temp_timeslot;
                    temp_timeslot.dow_ = course_dow;
                    temp_timeslot.start_time_ = course_start_date;
                    temp_timeslot.end_time_ = course_end_date;
                    timeslot.push_back(temp_timeslot);
                }
                std::cout << "Enter the course location: ";
                std::cin >> course_location;
                std::cout << "Enter the instructor's name: ";
                std::cin.ignore();
                std::getline(std::cin, instructor_name);
                std::cout << "Enter the maximum number of students that can register: ";
                std::cin >> max_students;

                Course course(course_name, course_id, course_section, course_credits, sessions_per_week, timeslot, course_start_date, course_end_date, course_location, instructor_name, max_students, 0, student_list);
                course_list_.push_back(course);
                std::cout << "The following new course has been successfully added!" << std::endl;
                course.AdminPrint();
                return;
            }
        }
    }
}

void Admin::EditCourse() {
    std::string edit_course_id;
    int edit_course_section;
    std::cout << "Enter the ID of the course you would like to modify: ";
    std::cin >> edit_course_id;
    std::cout << "Enter the section of the course you would like to modify: ";
    std::cin >> edit_course_section;
    
    for(int i = 0; i < course_list_.size(); ++i) {
        Course course_ith = course_list_[i];
        if(course_ith.GetCourseID() == edit_course_id && course_ith.GetCourseSection() == edit_course_section) {
            bool course_edit_flag = true;
            while(course_edit_flag) {
                course_list_[i].AdminPrint();
                std::string option;
                std::cout << "Here are your options: ";
                std::cout << "\n1. Course name";
                std::cout << "\n2. Course ID";
                std::cout << "\n3. Course number of credits";
                std::cout << "\n4. Course section";
                std::cout << "\n5. Course start date";
                std::cout << "\n6. Course end date";
                std::cout << "\n7. Course location";
                std::cout << "\n8. Course instructor";
                std::cout << "\n9. Course maximum number of students";
                std::cout << "\n0. Exit";
                std::cout << "Please enter your choice: ";
                std::cin >> option; 
                while(option != "0" && option != "1" && option != "2" && option != "3" && option != "4" && option != "5" && option != "6" && option != "7" && option != "8" && option != "9") {
                    std::cout << "Please enter a valid number.";
                    std::string option1; std::cin >> option1;
                    option = option1;
                }
                if(option == "0") {
                    // Exit
                    course_edit_flag = false;
                }
                else if(option == "1") {
                    // Modify course_name_
                    std::string course_name;
                    std::cout << "Enter the course's new name: ";
                    std::cin >> course_name;
                    course_list_[i].SetCourseName(course_name);
                    std::cout << "Course name has been successfully edited to: " << course_name << std::endl;
                }
                else if(option == "2") {
                    // Modify course_id_
                    std::string course_id;
                    std::cout << "Enter the course's new ID: ";
                    std::cin >> course_id;
                    for(int j = 0; j < course_list_.size(); ++j) {
                        if(course_list_[j].GetCourseID() == course_id) {
                            if(course_list_[j].GetCourseSection() == edit_course_section) {
                                std::cout << "The course which has course ID " << course_id << " and course section " << edit_course_section << " exists!" << std::endl;
                                course_list_[j].AdminPrint();
                            }
                        }
                        else {
                            course_list_[i].SetCourseID(course_id);
                            std::cout << "Course ID has been successfully edited to: " << course_id << std::endl;
                        }
                    }
                }
                else if(option == "3") {
                    // Modify course_credits_
                    int course_credits;
                    std::cout << "Enter the course's new number of credits: ";
                    std::cin >> course_credits;
                    course_list_[i].SetCourseCredits(course_credits);
                    std::cout << "Course number of credits has been successfully edited to: " << course_credits << std::endl;
                }
                else if(option == "4") {
                    // Modify course_section_
                    int course_section;
                    std::cout << "Enter the course's new section: ";
                    std::cin >> course_section;
                    course_list_[i].SetCourseSection(course_section);
                    std::cout << "Course section has been successfully edited to: " << course_section << std::endl;
                }

                else if(option == "5") {
                    // Modify course_start_date_
                    std::string course_start_date;
                    std::cout << "Enter the course's new start date: ";
                    std::cin >> course_start_date;
                    course_list_[i].SetCourseStartDate(course_start_date);
                    std::cout << "Course start date has been successfully edited to: " << course_start_date << std::endl;
                }
                else if(option == "6") {
                    // Modify course_end_date_
                    std::string course_end_date;
                    std::cout << "Enter the course's new end date: ";
                    std::cin >> course_end_date;
                    course_list_[i].SetCourseEndDate(course_end_date);
                    std::cout << "Course end date has been successfully edited to: " << course_end_date << std::endl;
                }
                else if(option == "7") {
                    // Modify course_location_
                    std::string course_location;
                    std::cout << "Enter the course's new location: ";
                    std::cin >> course_location;
                    course_list_[i].SetCourseLocation(course_location);
                    std::cout << "Course location has been successfully edited to: " << course_location << std::endl;
                }
                else if(option == "8") {
                    // Modify instructor_name_
                    std::string instructor_name;
                    std::cout << "Enter the new instructor's name: ";
                    std::cin >> instructor_name;          
                    course_list_[i].SetInstructorName(instructor_name);
                    std::cout << "Course instructor name has been successfully edited to: " << instructor_name << std::endl;
                }
                else if(option == "9") {
                    // Modify max_students_
                    int max_students;
                    std::cout << "Enter the new number of maximum students: ";
                    std::cin >> max_students;
                    course_list_[i].SetMaxStudents(max_students);
                    std::cout << "Course number of maximum students has been successfully edited to: " << max_students << std::endl;
                }
            }
            return;
        }

        else {
            if(i == course_list_.size() - 1) {
                std::cout << "Oops! We are unable to find that course, let's try again!" << std::endl;
                EditCourse();
            }
        }
    }
}



void Admin::DeleteCourse() {
    std::string delete_course_id;
    int delete_course_section;
    std::cout << "Enter the ID of the course you would like to delete: ";
    std::cin >> delete_course_id;
    std::cout << "Enter the section of the course you would like to delete: ";
    std::cin >> delete_course_section;

    for(int i = 0; i < course_list_.size(); ++i) {
        Course course_ith = course_list_[i];
        if(course_ith.GetCourseID() == delete_course_id && course_ith.GetCourseSection() == delete_course_section) {
            course_list_.erase(course_list_.begin() + i);
            std::cout << "The following course has been successfully removed!" << std::endl;
            course_ith.AdminPrint();
            return;
        } 
        else { 
            if(i == course_list_.size() - 1) {
                std::cout << "Oops! We are unable to find that course, let's try again!" << std::endl;
                DeleteCourse();
            }
        }
    }
}

void Admin::RegisterStudent() {
    std::string id;
    std::cout << "Enter the new student's ID: ";
    std::cin >> id;
    std::string first_name;
    std::string last_name;
    std::cout << "Enter the new student's first name: ";
    std::cin >> first_name;
    std::cout << "Enter the new student's last name: ";
    std::cin >> last_name;

    Student new_student(first_name, last_name, id, id, id);
    master_registry_.push_back(new_student);
    std::cout << "Following new student has been successfully added!" << std::endl;
    std::cout << "First name: " << master_registry_[master_registry_.size() - 1].GetFirstName() << std::endl << "Last name: " << master_registry_[master_registry_.size() - 1].GetLastName() << std::endl << "Student ID: " << master_registry_[master_registry_.size() - 1].GetID() << std::endl << "Student's username: " << master_registry_[master_registry_.size() - 1].GetUsername() << std::endl << "Student's password: " << master_registry_[master_registry_.size() - 1].GetPassword() << std::endl;
    std::cout << std::endl;
}

void Admin::ViewACourse() {
    std::string course_id;
    std::cout << "Enter the course ID: ";
    std::cin >> course_id;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCourseID() == course_id) {
            course_list_[i].AdminPrint();
        }
    }
}

void Admin::ViewAllCourses() {
    for(int i = 0; i < course_list_.size(); ++i) {
        course_list_[i].AdminPrint();
    }
}

std::vector<Course> Admin::ViewUnavailableCourses() {
    std::vector<Course> return_courses;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCurrentStudents() == course_list_[i].GetMaxStudents()) {
            course_list_[i].AdminPrint();
            return_courses.push_back(course_list_[i]);            
        }
    }
    return return_courses;
}

std::vector<Course> Admin::ViewAvailableCourses() {
    std::vector<Course> return_courses;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCurrentStudents() != course_list_[i].GetMaxStudents()) {
            course_list_[i].AdminPrint();
            return_courses.push_back(course_list_[i]);
        }
    }
    return return_courses;
}


void Admin::ViewRegisteredStudents() {
    std::string course_name;
    std::cout << "Enter the course's name: ";
    std::cin >> course_name;
    for (int i = 0; i < course_list_.size(); ++i) {
        if (course_list_[i].GetCourseName() == course_name) {
            for (int j = 0; j < course_list_[i].GetStudentList().size(); ++j) {
                std::string first_name = course_list_[i].GetStudentList()[j].GetFirstName();
                std::string last_name = course_list_[i].GetStudentList()[j].GetLastName();
                std::cout << "Registered Students for " << course_name << std::endl;
                std::cout << "* " << first_name << " " << last_name;
            }
        }
    }
}
void Admin::ViewAllStudentCourses() {
    std::string first_name;
    std::string last_name;
    std::cout << "Enter the new student's first name: ";
    std::cin >> first_name;
    std::cout << "Enter the new student's last name: ";
    std::cin >> last_name;
    std::cout << first_name << " " << last_name << "'s Classes:" << std::endl;
    for (int i = 0; i < course_list_.size(); ++i) {
        for (int j = 0; j < course_list_[i].GetStudentList().size(); ++j) {
            if (course_list_[i].GetStudentList()[j].GetFirstName() == first_name && course_list_[i].GetStudentList()[j].GetLastName() == last_name) {
                std::string course_name = course_list_[i].GetCourseName();
                std::cout << "* " << course_name << std::endl;
            }
        }
    }
}


void Admin::SortCourses() {
    bool course_sort_flag = true;
    while(course_sort_flag) {
        std::string option;
        std::cout << "Here are your options: ";
        std::cout << "\n1. Course name";
        std::cout << "\n2. Course ID";
        std::cout << "\n3. Course number of credits";
        std::cout << "\n4. Course section";
        std::cout << "\n5. Course maximum number of students";
        std::cout << "\n6. Course current number of students";
        std::cout << "\n0. Exit";
        std::cout << "\nPlease enter your choice: ";
        std::cin >> option; 
        while(option != "0" && option != "1" && option != "2" && option != "3" && option != "4" && option != "5" && option != "6") {
            std::cout << "Please enter a valid number.";
            std::string option1; std::cin >> option1;
            option = option1;
        }
        if(option == "0") {
            // Exit
            course_sort_flag = false;
        }
        else if(option == "1") {
            // Sort by course_name_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetCourseName() < course2.GetCourseName(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
        else if(option == "2") {
            // Sort by course_id_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetCourseID() < course2.GetCourseID(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
        else if(option == "3") {
            // Sort by course_credits_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetCourseCredits() < course2.GetCourseCredits(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
        else if(option == "4") {
            // Sort by course_section_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetCourseSection() < course2.GetCourseSection(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
        else if(option == "5") {
            // Sort by max_students_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetMaxStudents() < course2.GetMaxStudents(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
        else if(option == "6") {
            // Sort by current_students_
            std::sort(course_list_.begin(), course_list_.end(), [](Course &course1, Course &course2){ return course1.GetCurrentStudents() < course2.GetCurrentStudents(); });
            for (int i = 0; i < course_list_.size(); ++i) {
                course_list_[i].AdminPrint();
            }
        }
    }
}
void Admin::WriteToFileFullCourses() {
    std::string file_name = "text.txt";
    std::ofstream file_writer(file_name);
    if (file_writer.is_open()) {
        for (int i = 0; i < ViewUnavailableCourses().size(); ++i) {
            std::string text = ViewUnavailableCourses()[i].AdminPrint();
            file_writer << text << std::endl;
        }
        file_writer.close();
    } else {
        std::cout << "Error writing file '" << file_name << "'" << std::endl;
    }
}
