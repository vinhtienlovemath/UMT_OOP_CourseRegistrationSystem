#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>

#include "../include/Course.h"
#include "../include/User.h"
#include "../include/Student.h"
#include "../include/Admin.h"
#include "../src/Course.cpp"
#include "../src/User.cpp"
#include "../src/Student.cpp"
#include "../src/Admin.cpp"

int main() {
    std::vector<Course> course_list;
    std::vector<Student> student_list;

    std::fstream course_file;
    course_file.open("../data/Courses.csv");
    std::fstream timeslot_file;
    timeslot_file.open("../data/CourseTimeslot.csv");
    std::fstream student_file;
    student_file.open("../data/Students.csv");
    std::fstream registration_file;
    registration_file.open("../data/Registration.csv");
    
    if(!student_file) {
        std::cout << "Error";
        student_file.close();
    }
    else {
        std::string student_line = "";
        while(std::getline(student_file, student_line)) {
            std::string student_first_name, student_last_name, student_id, student_username, student_password;

            std::stringstream input_student_line(student_line);
            std::getline(input_student_line, student_id, ',');
            std::getline(input_student_line, student_first_name, ',');
            std::getline(input_student_line, student_last_name, ',');
            std::getline(input_student_line, student_username, ',');
            std::getline(input_student_line, student_password);

            Student student(student_first_name, student_last_name, student_username, student_password, student_id);
            student_list.push_back(student);
        }
        student_file.close();
    }
    
    
    if(!course_file) {
        std::cout << "Error";
        course_file.close();
    }
    else {
        if(!timeslot_file) {
            std::cout << "Error";
            timeslot_file.close();
        }
        else {
            std::string course_line = "";
            while(std::getline(course_file, course_line)) {
                std::string course_name;
                std::string course_id;
                int course_credits;
                int course_section;
                int sessions_per_week;
                std::vector<Timeslot> timeslot;
                std::string course_start_date;
                std::string course_end_date;
                std::string course_location;
                std::string instructor_name;
                int max_students;
                int current_students;
                std::vector<Student> course_student_list;

                std::stringstream input_course_line(course_line);
                std::string temp_string;
                std::getline(input_course_line, course_name, ',');
                std::getline(input_course_line, course_id, ',');
                std::getline(input_course_line, temp_string, ','); course_section = atoi(temp_string.c_str());
                std::getline(input_course_line, temp_string, ','); course_credits = atoi(temp_string.c_str());
                std::getline(input_course_line, temp_string, ','); sessions_per_week = atoi(temp_string.c_str());
                std::getline(input_course_line, course_start_date, ',');
                std::getline(input_course_line, course_end_date, ',');
                std::getline(input_course_line, course_location, ',');
                std::getline(input_course_line, instructor_name, ',');
                std::getline(input_course_line, temp_string, ','); max_students = atoi(temp_string.c_str());
                std::getline(input_course_line, temp_string); current_students = atoi(temp_string.c_str());

                std::string timeslot_line = "";
                while(std::getline(timeslot_file, timeslot_line)) {
                    Timeslot temp_timeslot;
                    std::string course_dow, course_start_time, course_end_time;
                    std::string course_id_fk;
                    int course_section_fk;
                    std::stringstream input_timeslot_line(timeslot_line);
                    std::getline(input_timeslot_line, course_dow, ',');
                    std::getline(input_timeslot_line, course_start_time, ',');
                    std::getline(input_timeslot_line, course_end_time, ',');
                    std::getline(input_timeslot_line, course_id_fk, ',');
                    std::getline(input_timeslot_line, temp_string); course_section_fk = atoi(temp_string.c_str());
                    if(course_id == course_id_fk && course_section == course_section_fk) {
                        temp_timeslot.dow_ = course_dow;
                        temp_timeslot.start_time_ = course_start_time;
                        temp_timeslot.end_time_ = course_end_time;
                        timeslot.push_back(temp_timeslot);
                    }   
                }

                std::string registration_line = "";
                while(std::getline(registration_file, registration_line)) {
                    std::string student_id;
                    std::string course_id_fk;
                    int course_section_fk;

                    std::stringstream input_registration_line(registration_line);
                    std::string temp_registration_string;
                    std::getline(input_registration_line, student_id, ',');
                    std::getline(input_registration_line, course_id_fk, ',');
                    std::getline(input_registration_line, temp_registration_string); course_section_fk = atoi(temp_registration_string.c_str());
                    if(course_id == course_id_fk && course_section == course_section_fk) {
                        for(Student student: student_list) {
                            if(student.GetID() == student_id) {
                                course_student_list.push_back(student);
                            }
                        }
                    }   
                }
                Course course(course_name, course_id, course_section, course_credits, sessions_per_week, timeslot, course_start_date, course_end_date, course_location, instructor_name, max_students, current_students, course_student_list);
                course_list.push_back(course);
            }
            course_file.close();
            timeslot_file.close();
        }
    }

    bool w = true;
    while(w) {
        system("cls");
        std::cout << "Welcome to UMT Course Registration System. Are you admin (a) or student (s)?\n";
        char s; std::cin >> s;
        while(s != 'a' && s != 's' && s != 'e') {
            std::cout << "Please enter 'a' for admin or 's' for student.\n";
            char s1; std::cin >> s1;
            s = s1;
        }
        
        bool h = true;
        if(s == 'a') {
            system("cls");
            Admin admin("admin", "admin1", "A001");
            admin.SetCourseList(course_list);
            std::cout << "Please enter your username.\n";
            std::string username; std::cin >> username;
            while(username != admin.GetUsername()) {
                std::cout << "Invalid username. Please enter your username again.\n";
                std::string username1; std::cin >>username1;
                username = username1;
            }
            system("cls");
            std::cout << "Please enter your password.\n";
            std::string password; std::cin >> password;
            while(password != admin.GetPassword()) {
                std::cout << "Incorrect password. Please enter your password again.\n";
                std::string password1; std::cin >> password1;
                password = password1;
            }
            
            while(h) {
                system("cls");
                std::cout << "Welcome admin. Here are your options:";
                std::cout << "\n1. Create";
                std::cout << "\n2. Edit";
                std::cout << "\n3. Delete";
                std::cout << "\n4. Display a course";
                std::cout << "\n5. Display full course";
                std::cout << "\n6. Sort courses";
                std::cout << "\n0. Exit";
                std::cout << "\nPlease enter the number of your selection.";
                std::string choice; std::cin >> choice;
                while(choice != "0" && choice != "1" && choice != "2" && choice != "3" && choice != "4" && choice != "5" && choice != "6") {
                    std::cout << "Please enter a valid number.";
                    std::string choice1; std::cin >> choice1;
                    choice = choice1;
                }
                if(choice == "0") {
                    course_list = admin.GetCourseList();
                    h = false;
                }
                else if(choice == "1") {
                    system("cls");
                    admin.CreateCourse();
                    system("pause");
                }
                else if(choice == "2") {
                    system("cls");
                    admin.EditCourse();
                    system("pause");
                }
                else if(choice == "3") {
                    system("cls");
                    admin.DeleteCourse();
                    system("pause");
                }
                else if(choice == "4") {
                    system("cls");
                    admin.ViewACourse();
                    system("pause");
                }
                else if(choice == "5") {
                    system("cls");
                    admin.ViewAllCourses();
                    system("pause");
                }
                else if(choice == "6") {
                    system("cls");
                    admin.SortCourses();
                    system("pause");
                }
                // else if(choice == "7") {
                //     system("cls");
                //     admin.RegisterStudent();
                //     system("pause");
                // }
            }
        }
        
        bool f = true;
        if(s == 's') {
            bool c = true;
            Student current_student;
            system("cls");
            std::cout << "Please enter your username.";
            std::string username; std::cin >> username;
            while(c) {
                for(int i = 0; i < student_list.size(); ++i) {
                    std::string temp_username = student_list[i].GetUsername();
                    if(temp_username == username) {
                        current_student = student_list[i];
                        c = false;
                    }
                }
                if(c == true) {
                    std::cout << "Invalid username. Please enter again.";
                    std::string username1; std::cin >> username1;
                    username = username1;
                }
            }

            std::cout << "Please enter your password.";
            std::string password; std::cin >> password;
            while(!c) {
                    std::string temp_password = current_student.GetPassword();
                    if(temp_password == password) {
                        c = true;
                    }
                if(c == false) {
                    std::cout << "Incorrect password. Please enter again.";
                    std::string password1; std::cin >> password1;
                    password = password1;
                }
            }
            current_student.SetCourseList(course_list);
            while(f) {
                
                system("cls");
                std::cout << "Welcome student. Here are your options:";
                std::cout << "\n1. View all course";
                std::cout << "\n2. View available course";
                std::cout << "\n3. View unavailable course";
                std::cout << "\n4. Register to course";
                std::cout << "\n5. Withdraw from course";
                std::cout << "\n6. View all registered course";
                std::cout << "\n0. Exit";
                std::cout << "\nPlease enter the number of your selection.";
                char choice; std::cin >> choice;
                while(choice != '0' && choice != '1' && choice != '2' && choice != '3' && choice != '4' && choice != '5' && choice != '6') {
                    std::cout << "Please enter a valid number.";
                    char choice1; std::cin >> choice1;
                    choice = choice1;
                }
                if(choice == '0') {
                    course_list = current_student.GetCourseList();
                    f = false;
                }
                else if(choice == '1') {
                    system("cls");
                    current_student.ViewAllCourses();
                    system("pause");
                }
                else if(choice == '2') {
                    system("cls");
                    current_student.ViewAvailableCourses();
                    system("pause");
                }
                else if(choice == '3') {
                    system("cls");
                    current_student.ViewAvailableCourses();
                    system("pause");
                }
                else if(choice == '4') {
                    system("cls");
                    current_student.RegisterToCourse();
                    system("pause");
                }
                else if(choice == '5') {
                    system("cls");
                    current_student.WithdrawFromCourse();
                    system("pause");
                }
                else if(choice == '6') {
                    system("cls");
                    current_student.ViewAllRegisteredCourses();
                    system("pause");
                }
            }
        }
        if(s == 'e') {
            w = false;
        }
    }
    
    return 0;
}