#include "../include/User.h"

User::User() {}

User::User(std::string id, std::string username, std::string password) {
    this->username_ = username;
    this->password_ = password;
    this->id_ = id;
}

std::string User::GetID(){
    return id_;
}

void User::SetID(std::string id) {
    this->id_ = id;
}


std::string User::GetUsername() {
    return username_;
}

void User::SetUsername(std::string username) {
    this->username_ = username;
}

std::string User::GetPassword() {
    return password_;
}

void User::SetPassword(std::string password) {
    this->password_ = password;
}

void User::SetCourseList(std::vector<Course> course_list) {
    this->course_list_ = course_list;
}

std::vector<Course> User::GetCourseList() {
    return course_list_;
}