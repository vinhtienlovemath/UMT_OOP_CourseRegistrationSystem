#include "../include/Student.h"

Student::Student() { }

Student::Student(std::string first_name, std::string last_name, std::string username, std::string password, std::string id): User(id, username, password), first_name_(first_name), last_name_(last_name) {}

std::string Student::GetFirstName() {
    return first_name_;
}

void Student::SetFirstName(std::string first_name) {
    this->first_name_ = first_name;
}

std::string Student::GetLastName() {
    return last_name_;
}

void Student::SetLastName(std::string last_name) {
    this->last_name_ = last_name;
}

std::string Student::GetID() {
    return id_;
}

void Student::SetID(std::string id) {
    this->id_ = id;
}

void Student::ViewACourse() {
    std::string course_id;
    std::cout << "Enter the course ID: ";
    std::cin >> course_id;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCourseID() == course_id) {
            course_list_[i].StudentPrint();
        }
    }
}
void Student::ViewAllCourses() {
    for(int i = 0; i < course_list_.size(); ++i) {
        course_list_[i].StudentPrint();
    }
}

std::vector<Course> Student::ViewUnavailableCourses() {
    std::vector<Course> return_courses;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCurrentStudents() == course_list_[i].GetMaxStudents()) {
            course_list_[i].StudentPrint();
            return_courses.push_back(course_list_[i]);            
        }
    }
    return return_courses;
}

std::vector<Course> Student::ViewAvailableCourses() {
    std::vector<Course> return_courses;
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCurrentStudents() != course_list_[i].GetMaxStudents()) {
            course_list_[i].StudentPrint();
            return_courses.push_back(course_list_[i]);
        }
    }
    return return_courses;
}

void Student::RegisterToCourse() {
    int course_section;
    std::string course_id;
    std::cout << "Enter the course ID: ";
    std::cin >> course_id;
    std::cout << "Enter the course section ID: ";
    std::cin >> course_section;
    
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCourseID() == course_id && course_list_[i].GetCourseSection() == course_section) {
            if(course_list_[i].GetCurrentStudents() < course_list_[i].GetMaxStudents()) {
                Student student(first_name_, last_name_, username_, password_, id_);
                course_list_[i].GetStudentList().push_back(student);
                registered_course_.push_back(course_list_[i]);
                course_list_[i].SetCurrentStudents(course_list_[i].GetCurrentStudents() + 1);

                std::cout << "You have been successfully added to the course!" << std::endl;
                for(Student student: course_list_[i].GetStudentList()) std::cout << student.GetID();
            }
            else {
                std::cout << "Course is full. Please choose other courses!\n";
            }
        }
    }
}

void Student::WithdrawFromCourse() {
    int course_section;
    std::string course_id;
    std::cout << "Enter the course ID: ";
    std::cin >> course_id;
    std::cout << "Enter the course section ID: ";
    std::cin >> course_section;
    
    for(int i = 0; i < course_list_.size(); ++i) {
        if(course_list_[i].GetCourseID() == course_id && course_list_[i].GetCourseSection() == course_section) {
            Student student(first_name_, last_name_, username_, password_, id_);
            for(int j = 0; j < course_list_[i].GetStudentList().size(); ++j) {
                if(course_list_[i].GetStudentList()[j].id_ == id_) {
                    course_list_[i].GetStudentList().erase(course_list_[i].GetStudentList().begin() + j);
                }
            }
            for(int j = 0; j < registered_course_.size(); ++j) {
                if(registered_course_[j].GetCourseID() == course_id && registered_course_[j].GetCourseSection() == course_section) {
                    registered_course_.erase(registered_course_.begin() + j);
                }
            }
            std::cout << "You have been successfully removed from the course!" << std::endl;
        }
    }
}

void Student::ViewAllRegisteredCourses() {
    for(Course course: registered_course_) {
        course.StudentPrint();
    }
}