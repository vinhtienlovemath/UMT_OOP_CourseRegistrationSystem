#include "../include/Course.h"

Course::Course() { }

Course::Course(std::string course_name, std::string course_id, int course_section, int course_credits, int sessions_per_week, std::vector<Timeslot> &timeslot, std::string course_start_date, std::string course_end_date, std::string course_location, std::string instructor_name, int max_students, int current_students, std::vector<Student> student_list) {
    this->course_name_ = course_name;
    this->course_id_ = course_id;
    this->course_credits_ = course_credits;
    this->course_section_ = course_section;
    this->sessions_per_week_ = sessions_per_week;
    this->timeslot_ = timeslot;
    this->course_start_date_ = course_start_date;
    this->course_end_date_ = course_end_date;
    this->course_location_ = course_location;
    this->instructor_name_ = instructor_name;
    this->max_students_ = max_students;
    this->current_students_ = current_students;
    this->student_list_ = student_list;
}

std::string Course::AdminPrint() {
    std::string names = "";
    std::string timeslots = "";
    for(int i = 0; i < sessions_per_week_; ++i) {
        std::string dow = timeslot_[i].dow_;
        std::string start_time = timeslot_[i].start_time_;
        std::string end_time = timeslot_[i].end_time_;
        timeslots = timeslots + dow + ", " + start_time + " - " + end_time + "; ";
    }

    if(!student_list_.empty()) {
        for(int i = 0; i < student_list_.size(); ++i) {
            std::string first_name = student_list_[i].GetFirstName();
            std::string last_name = student_list_[i].GetLastName();
            names = names + first_name + " " + last_name + ", ";
        }
        
        std::cout << "Course: " << course_name_ << '\n'
                  << "Course ID: " << course_id_ << '\n'
                  << "Section: " << course_section_ << '\n'
                  << "# of Credits: " << course_credits_ << '\n'
                  << "# of Session per Week: " << sessions_per_week_ << '\n'
                  << "Timeslot: " << timeslots << '\n'
                  << "Start Date: " << course_start_date_ << '\n'
                  << "End Date: " << course_end_date_ << '\n'
                  << "Location: " << course_location_ << '\n'
                  << "Instructor: " << instructor_name_ << '\n'
                  << "Maximum # of Students: " << max_students_ << '\n' 
                  << "Current # of Students: " << current_students_ << '\n'
                  << "Registered Students: " << names << '\n';
        std::cout << "==========" << '\n';

        std::string text1 = "Course: " + course_name_ + "\n" 
                          + "Course ID: " + course_id_ + "\n" 
                          + "Section: " + std::to_string(course_section_) + "\n"
                          + "# of Credits: " + std::to_string(course_credits_) + "\n"
                          + "# of Session per Week: " + std::to_string(sessions_per_week_) + "\n"
                          + "Timeslot: " + timeslots + "\n"
                          + "Start Date: " + course_start_date_ + "\n"
                          + "End Date: " + course_end_date_ + "\n"
                          + "Location: " + course_location_ + "\n"
                          + "Instructor: " + instructor_name_ + "\n"
                          + "Maximum # of Students: " + std::to_string(max_students_) + "\n" 
                          + "Current # of Students: " + std::to_string(current_students_) + "\n" 
                          + "Registered Students: " + names + "\n" ;
        return text1;
    } 
    
    else {
        std::cout << "Course: " << course_name_ << '\n'
                  << "Course ID: " << course_id_ << '\n'
                  << "Section: " << course_section_ << '\n'
                  << "# of Credits: " << course_credits_ << '\n'
                  << "# of Session per Week: " << sessions_per_week_ << '\n'
                  << "Timeslot: " << timeslots << '\n'
                  << "Start Date: " << course_start_date_ << '\n'
                  << "End Date: " << course_end_date_ << '\n'
                  << "Location: " << course_location_ << '\n'
                  << "Instructor: " << instructor_name_ << '\n'
                  << "Maximum # of Students: " << max_students_ << '\n' 
                  << "Current # of Students: " << current_students_ << '\n'
                  << "Registered Students: []" << '\n';
        std::cout << "==========" << '\n';
        std::string text2 = "Course: " + course_name_ + "\n" 
                          + "Course ID: " + course_id_ + "\n" 
                          + "Section: " + std::to_string(course_section_) + "\n"
                          + "# of Credits: " + std::to_string(course_credits_) + "\n"
                          + "# of Session per Week: " + std::to_string(sessions_per_week_) + "\n"
                          + "Timeslot: " + timeslots + "\n"
                          + "Start Date: " + course_start_date_ + "\n"
                          + "End Date: " + course_end_date_ + "\n"
                          + "Location: " + course_location_ + "\n"
                          + "Instructor: " + instructor_name_ + "\n"
                          + "Maximum # of Students: " + std::to_string(max_students_) + "\n" 
                          + "Current # of Students: " + std::to_string(current_students_) + "\n" 
                          + "Registered Students: []" + "\n" ;
        return text2;
    }
}

std::string Course::StudentPrint() {
    std::string timeslots = "";
    for(int i = 0; i < sessions_per_week_; ++i) {
        std::string dow = timeslot_[i].dow_;
        std::string start_time = timeslot_[i].start_time_;
        std::string end_time = timeslot_[i].end_time_;
        timeslots = timeslots + dow + ", " + start_time + " - " + end_time + "; ";
    }
    std::cout << "Course: " << course_name_ << '\n'
                << "Course ID: " << course_id_ << '\n'
                << "Section: " << course_section_ << '\n'
                << "# of Credits: " << course_credits_ << '\n'
                << "# of Session per Week: " << sessions_per_week_ << '\n'
                << "Timeslot: " << timeslots << '\n'
                << "Start Date: " << course_start_date_ << '\n'
                << "End Date: " << course_end_date_ << '\n'
                << "Location: " << course_location_ << '\n'
                << "Instructor: " << instructor_name_ << '\n'
                << "Maximum # of Students: " << max_students_ << '\n' 
                << "Current # of Students: " << current_students_ << '\n';
    std::cout << "==========" << '\n';
    std::string text = "Course: " + course_name_ + "\n" 
                        + "Course ID: " + course_id_ + "\n" 
                        + "Section: " + std::to_string(course_section_) + "\n"
                        + "# of Credits: " + std::to_string(course_credits_) + "\n"
                        + "# of Session per Week: " + std::to_string(sessions_per_week_) + "\n"
                        + "Timeslot: " + timeslots + "\n"
                        + "Start Date: " + course_start_date_ + "\n"
                        + "End Date: " + course_end_date_ + "\n"
                        + "Location: " + course_location_ + "\n"
                        + "Instructor: " + instructor_name_ + "\n"
                        + "Maximum # of Students: " + std::to_string(max_students_) + "\n" 
                        + "Current # of Students: " + std::to_string(current_students_) + "\n";
    return text;
}

std::string Course::GetCourseName()  {
    return course_name_;
}

void Course::SetCourseName(std::string course_name) {
    this->course_name_ = course_name;
}

std::string Course::GetCourseID()  {
    return course_id_;
}

void Course::SetCourseID(std::string course_id) {
    this->course_id_ = course_id;
}

int Course::GetMaxStudents()  {
    return max_students_;
}

void Course::SetMaxStudents(int max_students) {
    this->max_students_ = max_students;
}

int Course::GetCurrentStudents()  {
    return current_students_;
}

void Course::SetCurrentStudents(int current_students) {
    this->current_students_ = current_students;
}

std::vector<Student> Course::GetStudentList() {
    return student_list_;
}

void Course::SetStudentList(std::vector<Student> student_list) {
    this->student_list_ = student_list;
}

std::string Course::GetInstructorName()  {
    return instructor_name_;
}

void Course::SetInstructorName(std::string instructor_name) {
    this->instructor_name_ = instructor_name;
}

int Course::GetCourseSection()  {
    return course_section_;
}

void Course::SetCourseSection(int course_section) {
    this->course_section_ = course_section;
}

std::string Course::GetCourseLocation()  {
    return course_location_;
}

void Course::SetCourseLocation(std::string course_location) {
    this->course_location_ = course_location;
}

int Course::GetCourseSessionsPerWeek() {
    return sessions_per_week_;
}

void Course::SetCourseSessionsPerWeek(int sessions_per_week) {
    this->sessions_per_week_ = sessions_per_week;
}

std::vector<Timeslot> Course::GetCourseTimeslot() {
    return timeslot_;
}

void Course::SetCourseTimeslot(std::vector<Timeslot> timeslot) {
    this->timeslot_ = timeslot;
}

int Course::GetCourseCredits() {
    return course_credits_;
} 

void Course::SetCourseCredits(int course_credits) {
    this->course_credits_ = course_credits;
}

std::string Course::GetCourseStartDate() {
    return course_start_date_;
}
void Course::SetCourseStartDate(std::string course_start_date) {
    this->course_start_date_ = course_start_date;
}
std::string Course::GetCourseEndDate() {
    return course_end_date_;
}
void Course::SetCourseEndDate(std::string course_end_date) {
    this->course_end_date_ = course_end_date;
}