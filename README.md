# **Course Registration System - UMT Object-oriented Programming**
 
## **Introduction**

This is an C++ project for the Object-oriented Programming module's mid-term examination at Ho Chi Minh City University of Management and Technology, by Phan Vinh Tien - Student of School of Technology.

## **Demo**

The demo program file can be found at [../bin/main.exe](../bin/main.exe).