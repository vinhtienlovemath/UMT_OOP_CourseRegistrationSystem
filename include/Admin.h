#ifndef ADMIN_H
#define ADMIN_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include "Course.h"
#include "User.h"
#include "Student.h"    




class Admin: public User {
    private:
        std::vector<Student> master_registry_;

    public:
        Admin();
        Admin(std::string username, std::string password, std::string id);

        std::vector<Student> GetMasterRegistry();
        void SetUsername(std::vector<Student> master_registry);
        

        void CreateCourse();
        void DeleteCourse();
        void EditCourse();
        virtual void ViewACourse();
        virtual void ViewAllCourses();

        std::vector<Course> ViewUnavailableCourses();
        std::vector<Course> ViewAvailableCourses();
        void RegisterStudent();
        void ViewRegisteredStudents();
        void ViewAllStudentCourses();
        void SortCourses();
        void WriteToFileFullCourses();
};


#endif // ADMIN_H