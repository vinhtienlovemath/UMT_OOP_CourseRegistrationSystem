#ifndef COURSE_H
#define COURSE_H

#include <iostream>
#include <string>
#include <vector>
#include "Student.h"

struct Timeslot {
    std::string dow_, start_time_, end_time_;
};

class Course {
    protected:
        std::string course_name_;
        std::string course_id_;
        int course_credits_;
        int course_section_;
        int sessions_per_week_;
        std::vector<Timeslot> timeslot_;
        std::string course_start_date_;
        std::string course_end_date_;
        std::string course_location_;
        std::string instructor_name_;
        int max_students_;
        int current_students_;

        std::vector<Student> student_list_;


    public:
        Course();
        Course(std::string course_name, std::string course_id, int course_section, int course_credits, int sessions_per_week, std::vector<Timeslot> &timeslot, std::string course_start_date, std::string course_end_date, std::string course_location, std::string instructor_name, int max_students, int current_students, std::vector<Student> student_list);

        std::string AdminPrint();
        std::string StudentPrint();
        std::string GetCourseName();
        void SetCourseName(std::string course_name);
        std::string GetCourseID();
        void SetCourseID(std::string course_id);
        int GetCourseCredits();
        void SetCourseCredits(int course_credits);
        int GetCourseSection();
        void SetCourseSection(int course_section);
        int GetCourseSessionsPerWeek();
        void SetCourseSessionsPerWeek(int sessions_per_week);
        std::vector<Timeslot> GetCourseTimeslot();
        void SetCourseTimeslot(std::vector<Timeslot> timeslot);
        std::string GetCourseStartDate();
        void SetCourseStartDate(std::string course_start_date);
        std::string GetCourseEndDate();
        void SetCourseEndDate(std::string course_end_date);
        int GetMaxStudents();
        void SetMaxStudents(int max_students);
        int GetCurrentStudents();
        void SetCurrentStudents(int current_students);
        std::vector<Student> GetStudentList();
        void SetStudentList(std::vector<Student> student_list);
        std::string GetInstructorName();
        void SetInstructorName(std::string instructor_name);
        std::string GetCourseLocation();
        void SetCourseLocation(std::string course_location);
};

#endif // COURSE_H