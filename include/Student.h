#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>
#include <vector>
// #include <algorithm>
#include <string>
#include "User.h"
#include "Course.h"

class Student: public User {
    private:
        std::string first_name_;
        std::string last_name_;
        std::vector<Course> registered_course_;

    public:
        Student();
        Student(std::string first_name, std::string last_name, std::string username, std::string password, std::string id);
        
        std::string GetFirstName();
        void SetFirstName(std::string first_name);
        std::string GetLastName();
        void SetLastName(std::string last_name);
        void SetID(std::string id);
        std::string GetID();
        virtual void ViewACourse();
        virtual void ViewAllCourses();
        virtual std::vector<Course> ViewAvailableCourses();
        virtual std::vector<Course> ViewUnavailableCourses();
        void RegisterToCourse();
        void WithdrawFromCourse();
        void ViewAllRegisteredCourses();
};


#endif // STUDENT_H