#ifndef USER_H
#define USER_H

#include <iostream>
#include <string>
#include <vector>

class Course;

class User {
    protected:
        std::string username_;
        std::string password_;
        std::string id_;
        std::vector<Course> course_list_;


    public:
        User();
        User(std::string id, std::string username, std::string password); 

        std::string GetID();
        void SetID(std::string id);
        std::string GetUsername();
        void SetUsername(std::string username);
        std::string GetPassword();
        void SetPassword(std::string password);
        void SetCourseList(std::vector<Course> course_list);
        std::vector<Course> GetCourseList();
        virtual void ViewACourse() = 0;
        virtual void ViewAllCourses() = 0;
        virtual std::vector<Course> ViewAvailableCourses() = 0;
        virtual std::vector<Course> ViewUnavailableCourses() = 0;
};

#endif // USER_H